import os
import sys
import argparse
import subprocess
import platform
# -------------------------------------------------------------------------------------------------
# GLOBALS:
# -------------------------------------------------------------------------------------------------
PYUPDATE_VER = "1.1"
PYAGENT_DIR = "/usr/share/chartec"
HOSTNAME = platform.node()
PYAGENT_GIT = ["git", "-C", "/usr/share/chartec/", "clone", "https://rogerlawrence@bitbucket.org/rogerlawrence/pyagent_linux.git"]
# -------------------------------------------------------------------------------------------------
# Helper Functions
# -------------------------------------------------------------------------------------------------
def run(arg, output=False, debug=False):
    """ FUNCTION: run(<argument>)
        @param string arg
        @return response
    """
    try:
        response = os.popen(arg)
    except Exception as e:
        return str(e) if debug else False
    return response.read() if output else True

def run_go(command_list):
    """ FUNCTION run_go(<command>)
        @param list command_list
        @return output
    """
    try:
        response = subprocess.check_output(command_list)     
    except Exception as e:
        return e
    return response if response else True

def cron_updater():

    file_name = '/etc/cron.hourly/chartec'
    if not os.path.exists(file_name):
        f = open(file_name, 'a+')
        f.write('/usr/bin/python3 /usr/share/chartec/pyupdate.py --update pyagent')
        f.close()
        return True
    else:
        response = run_go(["rm", file_name])
        if not response:
            print("Error: Removing Cron File")
            return False
        else:
            f = open(file_name, 'a+')
            f.write('/usr/bin/python3 /usr/share/chartec/pyupdate.py --update pyagent')
            f.close()
            return True
    return           

def test_write():
    file_name = "/usr/share/chartec/test.txt"
    f = open(file_name, 'a+')
    f.write('1')
    f.close()

def sudoers_install(): 
    print("Setting Up Sudoers")
    if not os.path.exists("/usr/share/chartec/sudoers.txt"):
        command = 'echo "zabbix  ALL=(ALL) NOPASSWD:ALL" | sudo tee /etc/sudoers.d/zabbix'
        # If the file does not exist, run the sudo command & create file
        try:
            subprocess.run(command, shell=True, check=True)
            with open("sudoers.txt", "w") as file:
                file.write("echo \"zabbix  ALL=(ALL) NOPASSWD:ALL\" | sudo tee /etc/sudoers.d/zabbix\n")
            print("sudoers.txt file created successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Error creating sudoers.txt: {e}")
    else:
        print("sudoers.txt file already exists. Skipping.")

# -------------------------------------------------------------------------------------------------
# ARG PARSE:
# -------------------------------------------------------------------------------------------------
parser = argparse.ArgumentParser(description='CharTec Linux Agent Update / Installer')

parser.add_argument('--version', action='version', version=PYUPDATE_VER, help='version info')
parser.add_argument('--install', action='store', help='INSTALL Calls', dest='install')
parser.add_argument('--update', action='store', help='UPDATE Calls', dest='update')

# -------------------------------------------------------------------------------------------------
# End Arguments
# -------------------------------------------------------------------------------------------------
result = parser.parse_args()

# -------------------------------------------------------------------------------------------------
# INSTALL:
# -------------------------------------------------------------------------------------------------
if result.install:

    if result.install == "pyagent":
        
        print("Checking for Directory " + PYAGENT_DIR + "...")
        if not os.path.exists(PYAGENT_DIR):
            print("Creating Directory " + PYAGENT_DIR)
            os.makedirs(PYAGENT_DIR)
        
        print("Checking for GIT...")    
        git = os.path.exists("/usr/bin/git")
        if not git:
            print("Installing GIT...")
            response = run_go(["apt-get", "install", "git"])
            if not response:
                print("Error: Installing GIT")
                sys.exit()

            print("Ensuring GIT...")
            response = run_go(["apt-get", "-f", "install"])

        print("Installing PyAgent...")
        response = run_go(PYAGENT_GIT)
        if not response:
            print("Error: Installing PyAgent") 
            sys.exit()
        
        print("Setting Up Cron Job")
        response = cron_updater()
        if not response:
            print("Error: Creating Cron Files")

        sudoers_install()

        print("Complete")
        sys.exit()
    elif result.install == "git":
        response = run_go(["apt-get", "install", "git"])
        if not response:
            print("Error: Installing GIT")
        print("Complete")
    elif result.install == "test":
        test_write()
    elif result.install == "sudoers":
        sudoers_install()
        print("Complete")
    else:
        print("Error: Unknown Install Option")
        sys.exit()

# -------------------------------------------------------------------------------------------------
# UPDATE:
# -------------------------------------------------------------------------------------------------
if result.update:

    if result.update == "pyagent":
        print("Checking Installed CharTec PyAgent Version...")
        installed_version = run_go(["/usr/bin/python3", "/usr/share/chartec/pyagent_linux/pyagent.py", "--version"])
        print("Found pyAgent: " + installed_version.decode('utf-8'))
        if not installed_version:
            print("Error: Getting Installed PyAgent Version")
            sys.exit()
        
        print("Checking Latest CharTec PyAgent Version...")
        import urllib.request, json 
        with urllib.request.urlopen("https://api.chartec.net/agent/version/version") as url:
            data = json.loads(url.read().decode())
            version = data['pyagent_linux']
            print("Latest pyAgent: " + version)
        
        if float(installed_version) < float(version):
            print("Need to update pyAgent")
            response = run_go(["git", "-C", "/usr/share/chartec/pyagent_linux/", "pull", "-f"]) 
            if not response:
                print("Error: Pulling Updated PyAgent from GIT")
                sys.exit()
            run_go(["python3", "/usr/share/chartec/pyagent_linux/pyagent.py", "--install", "cronjob"])
        else:
            print("No Update Needed")
            sys.exit()
    
        sys.exit()
    else:
        print("Error: Unknown Update Option")
        sys.exit()

# ------------------------------------------------------------------------------------=
# CRON JOB:

